import React from 'react';
import { TabNavigator, StackNavigator } from 'react-navigation';
import First from '../screens/First';
import Second from '../screens/Second';
import Third from '../screens/Third';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import IconMaterialIcons from 'react-native-vector-icons/MaterialIcons';
import IconIonicons from 'react-native-vector-icons/Ionicons';

export default TabNav = TabNavigator({
  First: {
    screen: First,
    navigationOptions: () => ({
      tabBarIcon: ({ tintColor }) => {
        return (
          <IconFontAwesome 
            name='home'
            size={26}
            color={tintColor}
          />
        );
      }
    })
  },
  Second: {
    screen: Second,
    navigationOptions: () => ({
      tabBarIcon: ({ tintColor }) => {
        return (
          <IconMaterialIcons 
            name='favorite-border'
            size={26}
            color={tintColor}
          />
        );
      }
    })
  },
  Third: {
    screen: Third,
    navigationOptions: () => ({
      tabBarIcon: ({ tintColor }) => {
        return (
          <IconIonicons 
           name='md-contacts'
            size={26}
            color={tintColor}
          />
        );
      }
    })
  }
}, {
  tabBarOptions: {
    showIcon: true
  }
});