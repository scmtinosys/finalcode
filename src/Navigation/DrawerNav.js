import { createDrawerNavigator , createStackNavigator ,createAppContainer,DrawerItems } from 'react-navigation';
import React, { Component } from 'react';
import { SafeAreaView,View ,Image,ScrollView} from 'react-native'
import Fourth from '../screens/Fourth';
import Second from '../screens/Second';
import Third from '../screens/Third';
import DrawerScreen from './DrawerScreen'

import DrawerNav from './DrawerNav'
import Otp from './Otp'

const HomeStack = createStackNavigator({
  First:{
    screen:DrawerScreen
  },
  Second:{
    screen:Second,
  },
  Third: {
    screen: Otp,
  },
  Fourth: {
    screen: Fourth,
  },
 
});

const CustomDrawerComponent=(props)=>(
  <SafeAreaView style={{flex:1}}>
  <View style={{height:150,backgroundColor:'white',alignItems:'center',justifyContent:'center'}}>
    <Image source={require('../../assets/man.png')} style={{height:140,width:162}}/>
  </View>
  <ScrollView>
    <DrawerItems   {...props}/>
  </ScrollView>
  </SafeAreaView>
   
  )

export default Drawer = createDrawerNavigator ({
  'Your Trips': {
    screen: HomeStack
  },
  Help: {
    screen: HomeStack
  },
Payment: {
  screen: HomeStack
},
'Free Rides': {
  screen: HomeStack
},
Settings: {
  screen: HomeStack
},
'Log Out': {
  screen: HomeStack
},
},{
  contentComponent:CustomDrawerComponent,
  // drawerWidth:width-110,
  contentOptions:{
    activeTintColor:'black'
  },
  
    // initialRouteName: 'HomeStack',
  
  });