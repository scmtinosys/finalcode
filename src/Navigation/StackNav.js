import { StackNavigator } from 'react-navigation';
import First from '../screens/First';
import Second from '../screens/Second';

export default StackHome = StackNavigator({
  First: {
    screen: First,
    navigationOptions: () => ({
      title: 'Here is screen 1 !'
    })
  },
  Second: {
    screen: Second,
    navigationOptions: () => ({
      title: 'Here is Screen 2'
    })
  }
},{
  initialRouteName: 'DrawerScreen'
});