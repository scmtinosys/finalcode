import React, { Component } from 'react';
import { View,Text,StyleSheet,KeyboardAvoidingView,enabled,ScrollView,StatusBar,Button,TextInput,Alert,TouchableOpacity,Image,ImageBackground,Linking,Animated,Dimensions,Keyboard,Platform} from 'react-native';
import * as Animatable from'react-native-animatable'
import First from '../screens/First'
import {Icon} from 'native-base'
import DrawerNav from './DrawerNav'
import PhoneInput from 'react-native-phone-input';
import CountryPicker from 'react-native-country-picker-modal';
const SCREEN_HEIGHT=Dimensions.get('window').height

export default class DrawerScreen extends Component {
    
  static navigationOptions={
        headerTransparent: true,
        }

    constructor(props)
    {
        super(props);
  }

  componentWillMount(){
    this.loginHeight=new Animated.Value(150)
this.KeyboardWillShowListener=Keyboard.addListener('Keyboardwillshow',this.KeyboardWillShow)

this.KeyboardWillHideListener=Keyboard.addListener('Keyboardwillhide',this.KeyboardWillHide)

this.KeyboardDidShowListener=Keyboard.addListener('Keyboarddidshow',this.KeyboardWillShow)

this.KeyboardDidHideListener=Keyboard.addListener('KeyboardDidHide',this.KeyboardWillHide)
this.keyboardHeight=new Animated.Value(0)
this.forwardArrowOpacity=new Animated.Value(0)
this.borderBottomWidth=new Animated.Value(0)

this.borderBottom=new Animated.Value(0)
}
KeyboardWillShow=(event)=>{
    if(Platform.OS=='android')
    {
        duration=80
    }
    else{
        duration=event.duration
    }
Animated.parallel([
Animated.timing(this.keyboardHeight,{
    duration:duration+100,
    toValue:event.endCoordinates.height+10
}),
    Animated.timing(this.forwardArrowOpacity,{
        duration:duration,
        toValue:1
    }),
    Animated.timing(this.borderBottomWidth,{
        duration:duration,
        toValue:1
    }),
    Animated.timing(this.borderBottom,{
        duration:duration+100,
        toValue:1
    })
]).start()
}
KeyboardWillHide=(event)=>{
if(Platform.OS=='android')
{
    duration=80
}
if(Platform.OS=='ios')
{
    duration=80
}
else{
    duration=event.duration
}
Animated.parallel([
    Animated.timing(this.keyboardHeight,{
        duration:event.duration+100,
        toValue:0
    }),
        Animated.timing(this.forwardArrowOpacity,{
            duration:duration,
            toValue:0
        }),
        Animated.timing(this.borderBottomWidth,{
            duration:duration,
            toValue:0
        }),
        Animated.timing(this.borderBottom,{
            duration:event.duration+100,
            toValue:0
        })
]).start()

}



increaseHeightOfLogin=()=>{
   // this.setState({placeholderText:'092123456789'})
Animated.timing(this.loginHeight,{
toValue:SCREEN_HEIGHT,
duration:500
}).start(()=>{
this.refs.textInputMobile.focus()
})
}
decreaseHeightOfLogin=()=>{
    Keyboard.dismiss()
    Animated.timing(this.loginHeight,{
        toValue:150,
        duration:500
    }).start()
        }

      
    





state = {  }
render() { 
    
    
    return ( 
<KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
        
<View>
  <StatusBar backgroundColor="#000000" barStyle="light-content" />

 </View>
   
 <ImageBackground
       
        style={{flex:1}}>
         <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
         <Animatable.View 
         animation="zoomIn" iterationCount={1}
         style={{alignItems:'center',justifyContent:'center'}}>
           <Image source={require('../../assets/tino.png')} />
         </Animatable.View></View>
         <Animatable.View animation="slideInUp" iterationCount={1}>
             
        
                 

        
                 {/* </TouchableOpacity> */}
                 
                 {/* </Animated.View> */}
                 
                 {/* <TouchableOpacity> */}
                
                 </Animatable.View>
           
                 {/* <Icon 
               
                 title="Next"
                 onPress={() => {
    this.props.navigation.navigate('Login')
  }}
  />  */}
  
                  
                 
            
                 {/* </TouchableOpacity> */}
           
                 </ImageBackground>
          
            
               
                 <TouchableOpacity style={styles.button}>
          <Text
            style={styles.buttonText}
            onPress={() => this.props.navigation.navigate("Fourth")}
         
          >
            Login with Phone
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.button}>
          <Text
            style={styles.buttonText}
            onPress={() => this.props.navigation.navigate("Second")}
         
          >
            Login with Social
          </Text>
        </TouchableOpacity>
    
         
                     </KeyboardAvoidingView>  
         
         );
    }
}
 
const styles = StyleSheet.create({
    container: {
        flex: 0.85,
        alignItems: 'stretch',
        padding: 20,
       // paddingTop: 60,
    },

buttonText: {
    textAlign: "center",
    color: "#FFF",
    fontWeight: "100"
  },
    arrow:{
        paddingLeft:'90%',
    // marginTop:10
    },
   
button: {
    backgroundColor: "#27ae60",
    paddingVertical: 25,
    margin:30
  },
    arrows:{
        paddingRight:'50%',
    marginTop:-30
    }
  });
  