import React, { Component } from 'react';

import { SafeAreaView,View ,Image,ScrollView} from 'react-native'
import { TabNavigator, StackNavigator, DrawerNavigator,DrawerItems } from 'react-navigation';
import First from '../screens/First';
import Second from '../screens/Second';
import Third from '../screens/Third';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import IconMaterialIcons from 'react-native-vector-icons/MaterialIcons';
import IconIonicons from 'react-native-vector-icons/Ionicons';
import DrawerScreen from './DrawerScreen';


const Home = DrawerNavigator({
  Home: {
    screen: First,
    navigationOptions: () => ({
      title: 'Home',
      headerTintColor: 'yellow',
      headerStyle:{
        backgroundColor: '#4f3403',
        elevation: 0,
        showdowOpacity: 0
      },
      tabBarIcon: ({ tintColor }) => {
        return (
          <IconFontAwesome 
            name='home'
            size={26}
            color={tintColor}
          />
        );
      }
    })
  }
},{
  // headerMode: 'none'
});

const Favorite = StackNavigator({
  TabHome: First,
  Favorite: Second,
  Second: {
    screen: Second,
    navigationOptions: () => ({
      tabBarIcon: ({ tintColor }) => {
        return (
          <IconIonicons 
            name='md-contacts'
            size={26}
            color={tintColor}
          />
        );
      }
    })
  }
},{
  // headerMode: 'none'
});

const TabNav = TabNavigator({
  'Your Trips': Favorite,
  Help: Favorite,
 
  Payments: {
    screen: Favorite,
    navigationOptions: () => ({
      tabBarIcon: ({ tintColor }) => {
        return (
          <IconIonicons 
            name='md-contacts'
            size={26}
            color={tintColor}
          />
        );
      }
    })
  }
}, {
  tabBarOptions: {
    showIcon: true,
    style: {
      backgroundColor: '#0066ff',
      marginTop: 3,
      // showIcon: true,
    },
    inactiveTintColor: 'white',
    activeTintColor: '#663300'
  }
});

const CustomDrawerComponent=(props)=>(
  <SafeAreaView style={{flex:1}}>
  <View style={{height:150,backgroundColor:'white',alignItems:'center',justifyContent:'center'}}>
    <Image source={require('../../assets/man.png')} style={{height:140,width:162}}/>
  </View>
  <ScrollView>
    <DrawerItems   {...props}/>
  </ScrollView>
  </SafeAreaView>
   
  )
const DrawerTab = DrawerNavigator({
  'Your Trips': {
    screen: TabNav
  },
  Help: {
    screen: TabNav
  },
Payment: {
  screen: TabNav
},
'Free Rides': {
  screen: TabNav
},
Settings: {
  screen: TabNav
},
'Log Out': {
  screen: TabNav
},},
{
  contentComponent:CustomDrawerComponent,
  // drawerWidth:width-110,
  contentOptions:{
    activeTintColor:'black'
  },
  
});

export default Drawer = DrawerNavigator({
  Tabs: {
    screen: DrawerTab
  }
}, {
  contentComponent: props => <DrawerScreen {...props} />
});