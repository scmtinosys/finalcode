import React from 'react';
import { Button, View, Text,Dimensions } from 'react-native';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import First from './First'
import { CardViewWithImage } from "react-native-simple-card-view";
class Third extends React.Component {
  render() {
    const miniCardStyle = {shadowColor       : '#F6FBFB',shadowOffsetWidth : 2,shadowOffsetHeight: 2,shadowOpacity     : 0.1,shadowRadius      : 5,padding           : 5,margin            : 5,borderRadius      : 3,elevation         : 3,width             : (Dimensions.get("window").width / 2) - 10

  };
    return (
      <View style={{ flex: 0.6, alignItems: 'center', justifyContent: 'space-around' }}>
       
   
        
  <CardViewWithImage
 
        
        source={require('../../assets/fb.png')}
        title={ 'Facebook'}
        // onPress={this._fbAuth()}
        onPress={() => this.props.navigation.navigate('Third')}
        imageWidth={50}
        imageHeight={ 29 }
        roundedImage={ false }
        style={ miniCardStyle }
    />
          <CardViewWithImage
 
 source={require('../../assets/m.png')}
 title={ 'Google'}

 
 onPress={() => this.props.navigation.navigate('Third')}
 imageWidth={50}
 imageHeight={ 29 }
 roundedImage={ false }
 style={ miniCardStyle }
/>
<Text>By clicking it will go to social media</Text>
      </View>
    );
  }
}
export default Third;

// var React = require('react-native')
// var {
//   AppRegistry,
//   Text,
//   View,
//   MapView,
//   Dimensions,
// } = React

// class Third extends React.Component {
//   constructor(props) {
//     super(props)
//     this.state = {}
//     this.handleRegionChange = this.handleRegionChange.bind(this)
//   }
//   handleRegionChange(region) {
//     let lat = region.latitude
//     let lon = region.longitude
//     let latlng = lat + ',' + lon
//     this.props.onLatlngChange && this.props.onLatlngChange(latlng)
//   }
//   render() {
//     let style = {
//       position: 'absolute',
//       width: 0,
//       height: 0,
//       top: 0,
//       left: 0,
//       overflow: "visible",
//     }
//     let mapStyle = {
//       height: Dimensions.get('window').height,
//       width: Dimensions.get('window').width,
//     }
//     return (
//       <View style={style}>
//         <MapView style={mapStyle}
//           onRegionChange={this.handleRegionChange}
//           minDelta={0.01}
//           maxDelta={0.02}
//           showsUserLocation={true}
//           scrollEnabled={this.props.scroll}
//           zoomEnabled={this.props.zoom}
//           pitchEnabled={this.props.pitch}
//           rotationEnabled={this.props.rotate}
//           {...this.props} />
//       </View>
//     )
//   }
// }

// export default Third