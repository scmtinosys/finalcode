import React from 'react';
import { Button, View, Text,Dimensions } from 'react-native';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import First from './First'
import { CardViewWithImage } from "react-native-simple-card-view";
class Second extends React.Component {
  render() {
    const miniCardStyle = {shadowColor       : '#F6FBFB',shadowOffsetWidth : 2,shadowOffsetHeight: 2,shadowOpacity     : 0.1,shadowRadius      : 5,padding           : 5,margin            : 5,borderRadius      : 3,elevation         : 3,width             : (Dimensions.get("window").width / 2) - 10

  };
    return (
      <View style={{ flex: 0.6, alignItems: 'center', justifyContent: 'space-around' }}>
       
   
        
  <CardViewWithImage
 
        
        source={require('../../assets/fb.png')}
        title={ 'Facebook'}
        // onPress={this._fbAuth()}
        onPress={() => this.props.navigation.navigate('Third')}
        imageWidth={50}
        imageHeight={ 29 }
        roundedImage={ false }
        style={ miniCardStyle }
    />
          <CardViewWithImage
 
 source={require('../../assets/m.png')}
 title={ 'Google'}

 
 onPress={() => this.props.navigation.navigate('Third')}
 imageWidth={50}
 imageHeight={ 29 }
 roundedImage={ false }
 style={ miniCardStyle }
/>
<Text>By clicking it will go to social media</Text>
      </View>
    );
  }
}
export default Second;
