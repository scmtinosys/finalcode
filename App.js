/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import Stackhome from './src/Navigation/StackNav';
import DrawerNav from './src/Navigation/DrawerNav';
import DrawerScreen from './src/Navigation/DrawerScreen';
import First from './src/screens/First'
import TabNav from './src/Navigation/TabNav';
import MixedNavigators from './src/Navigation/MixedNavigators';
import StackNav from './src/Navigation/StackNav';
export default class App extends Component {
  render() {
    return (
    <DrawerNav/>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
